// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var txtAlturaCtrl = TextEditingController();
  var txtPesoCtrl = TextEditingController();
  String imageImc = "images/normal.jpg";
  double imc = 0;

  void calculaImc() {
    var altura = double.parse(txtAlturaCtrl.text);
    var peso = double.parse(txtPesoCtrl.text);

    imc = peso / (altura * altura);

    var image = 'normal';

    if(imc < 18.5)
      imageImc = "magreza";
    else if (imc < 25)
      imageImc = "normal";
    else if(imc < 30)
      imageImc = "images/sobrepeso.jpg";
    else if(imc < 35)
      imageImc = "images/obesidade.jpg";
    else
      imageImc = "images/obesidade2.jpg";

    setState(() {});

    FocusManager.instance.primaryFocus?.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.red,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('IMC'),
        ),
        body: Column(
          children: [
            Flexible(
              child: Container(
                margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
                child: Column(                  
                  children: [
                    TextField(
                      controller: txtAlturaCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'Altura',
                        labelText: 'Altura',
                      ),
                    ),
                    TextField(
                      controller: txtPesoCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'Peso',
                        labelText: 'Peso',
                      ),
                    ), 
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: calculaImc,
                        child: Text("Calcular IMC"),
                      ),
                    ),
                    Text(imc.toStringAsFixed(2)),
                  ],
                ),
              ),
            ),
            Flexible(
              flex: 1,
              child: Center(
                child: Image.asset(
                  imageImc,
                  scale: 0.5,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}